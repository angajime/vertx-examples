package io.vertx.example;

import java.util.Map;

/**
 * Created by tarazaky on 3/8/15.
 */
public class Thing {

    private String id;
    private Map<String, String> properties;

    public Thing(String id, Map<String, String> properties) {
        this.id = id;
        this.properties = properties;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
}
