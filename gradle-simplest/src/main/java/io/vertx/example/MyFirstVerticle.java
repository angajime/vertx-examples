package io.vertx.example;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;


/**
 * Created by tarazaky on 3/8/15.
 */
public class MyFirstVerticle extends AbstractVerticle {

    MongoClient mongo;

    @Override
    public void start(Future<Void> fut) {

        JsonObject config = ReadMongoConfig();

        mongo = MongoClient.createShared(vertx,config);


        // Create a router object.
        Router router = Router.router(vertx);

        // Bind "/" to our hello message - so we are still compatible.
        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response
                    .putHeader("content-type", "text/html")
                    .end("<h1>Hello from my first Vert.x 3 application</h1>");
        });

        router.post("/dweet/for/:thing").handler(this::dweetFor);
        router.get("/get/latest/dweet/for/:thing").handler(this::getLatestDweetFor);
        router.get("/get/dweets/for/:thing").handler(this::getDweetsFor);
        router.get("/listen/for/dweets/from/:thing").handler(this::listenForDweetsFrom);

        // Create the HTTP server and pass the "accept" method to the request handler.
        vertx
                .createHttpServer()
                .requestHandler(router::accept)
                .listen(
                        // Retrieve the port from the configuration,
                        // default to 8080.
                        config().getInteger("http.port", 8080),
                        result -> {
                            if (result.succeeded()) {
                                fut.complete();
                            } else {
                                fut.fail(result.cause());
                            }
                        }
                );
    }

    private JsonObject ReadMongoConfig() {
        Map<String, Object> config = new LinkedHashMap<>();

        config.put("db_name", "dwix_db");
        config.put("connection_string", "mongodb://localhost:27017");
        return new JsonObject(config);
    }

    private void listenForDweetsFrom(RoutingContext routingContext) {
        String id = routingContext.request().getParam("thing");
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
                mongo.count(id, new JsonObject(), res -> {
                    long count1 = res.result();
                    System.out.println("Count1: " + count1);
                    loop(id, routingContext, count1);
                });
        }
    }
    void loop(String id, RoutingContext routingContext, long count1){

            mongo.count(id, new JsonObject(), res2 -> {
                long count2 = res2.result();
                System.out.println("Count2: " + count2);
                if (count1 != count2) {
                    JsonObject query = new JsonObject();

                    FindOptions opt = new FindOptions();
                    opt.setSort(new JsonObject().put("created", -1));
                    opt.setLimit(1);

                    mongo.findWithOptions(id, query, opt, res3 -> {

                        if (res3.succeeded()) {
                            routingContext.response()
                                    .putHeader("Transfer-Encoding", "chunked")
                                    .setChunked(true)
                                    .putHeader("content-type", "application/json; charset=utf-8")
                                    .write(Json.encodePrettily(res3.result()))
                                    .write("\r\n\r\n");
                        } else {
                            res3.cause().printStackTrace();

                        }
                    });
                }
                loop(id, routingContext, count2);
            });
    }

    private void getLatestDweetFor(RoutingContext routingContext) {
        String id = routingContext.request().getParam("thing");
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            JsonObject query = new JsonObject();

            FindOptions opt = new FindOptions();
            opt.setSort(new JsonObject().put("created", -1));
            opt.setLimit(1);

            mongo.findWithOptions(id, query, opt, res -> {

                if (res.succeeded()) {
                    routingContext.response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(Json.encodePrettily(res.result()));
                } else {
                    res.cause().printStackTrace();

                }
            });
        }
    }

    private void dweetFor(RoutingContext routingContext) {

        String id = routingContext.request().getParam("thing");

        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {

            TimeZone tz = TimeZone.getTimeZone("UTC");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            df.setTimeZone(tz);
            String nowAsISO = df.format(new Date());

            JsonObject dweet = new JsonObject().put("created", new JsonObject().put("$date",nowAsISO));

            MultiMap params = routingContext.request().params();

            params.remove("thing");

            for (Map.Entry<String,String> entry : params.entries()){
                dweet.put(entry.getKey(),entry.getValue());
            }

            mongo.insert(id, dweet, res -> {
                if(!res.succeeded()){
                    res.cause().printStackTrace();
                }
            });

            routingContext.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(dweet));
        }
    }

    private void getDweetsFor(RoutingContext routingContext) {
        String id = routingContext.request().getParam("thing");
        // Buscamos en mongo la coleccion de id y la devolvemos entera.
        if (id == null) {
            routingContext.response().setStatusCode(400).end();
        } else {

            JsonObject query = new JsonObject();

            mongo.find(id, query, res -> {

                if (res.succeeded()) {
                    routingContext.response()
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(Json.encodePrettily(res.result()));
                } else {
                    res.cause().printStackTrace();

                }
            });


        }
    }

}
